<?php


namespace Chark\ApiBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * PanierElements
 *
 * @ORM\Table(name="panier_elements")
 * @ORM\Entity(repositoryClass="Chark\ApiBundle\Repository\PanierElementsRepository")
 */
class PanierElements
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Chark\ApiBundle\Entity\Panier", inversedBy="items")
     */
    private $panier;

    /**
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Chark\ApiBundle\Entity\Produit")
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;
    /**
     * @ORM\Column(type="decimal")
     */
    private $price;

    public function __construct(Panier $panier,Produit $produit,$quantite = 1)
    {
        $this->product = $produit;
        $this->panier = $panier;
        $this->quantite = $quantite;
        $this->price = $this->product->getPrixTtc();
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return PanierElements
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set price
     *
     * @return PanierElements
     */
    public function setPrice()
    {
        $this->price = $this->product->getPrixTtc();

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set panier
     *
     * @param Panier $panier
     *
     * @return PanierElements
     */
    public function setPanier(Panier $panier)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Get panier
     *
     * @return Panier
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * Set product
     *
     * @param Produit $product
     *
     * @return PanierElements
     */
    public function setProduct(Produit $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Produit
     */
    public function getProduct()
    {
        return $this->product;
    }
}
