<?php

namespace Chark\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Marque
 *
 * @ORM\Table(name="marque")
 * @ORM\Entity(repositoryClass="Chark\ApiBundle\Repository\MarqueRepository")
 * @Serializer\ExclusionPolicy("none")
 */
class Marque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_marque", type="string", length=255)
     * @Serializer\Groups({"Default"})
     */
    private $nomMarque;

    /**
     * @ORM\ManyToMany(targetEntity="Chark\ApiBundle\Entity\Produit", inversedBy="marques")
     * @Serializer\Exclude()
     */
    private $produits;

    /**
     * @Gedmo\Slug(fields={"nomMarque"})
     * @ORM\Column(type="string",unique=true)
     */
    private $slug;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomMarque
     *
     * @param string $nomMarque
     *
     * @return Marque
     */
    public function setNomMarque($nomMarque)
    {
        $this->nomMarque = $nomMarque;

        return $this;
    }

    /**
     * Get nomMarque
     *
     * @return string
     */
    public function getNomMarque()
    {
        return $this->nomMarque;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Marque
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get produits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Add produit
     *
     * @param \Chark\ApiBundle\Entity\Produit $produit
     *
     * @return Marque
     */
    public function addProduit(\Chark\ApiBundle\Entity\Produit $produit)
    {
        if(!$this->produits->contains($produit)){
            $this->produits[] = $produit;
            $produit->addMarque($this);
        }
        return $this;
    }

    /**
     * Remove produit
     *
     * @param \Chark\ApiBundle\Entity\Produit $produit
     * @return Marque
     */
    public function removeProduit(\Chark\ApiBundle\Entity\Produit $produit)
    {
        if($this->produits->contains($produit)){
            $this->produits->removeElement($produit);
            $produit->removeMarque($this);
        }
        return $this;

    }
}
