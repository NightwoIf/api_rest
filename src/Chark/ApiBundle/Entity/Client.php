<?php


namespace Chark\ApiBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table("oauth2_clients")
 * @ORM\Entity
 */
class Client extends BaseClient
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     *
     *
     */
    private $nom;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     *
     */
    private $prenom;
    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     *
     */
    private $adresse;
    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=false)
     *
     */
    private $email;

    /**
     * @ORM\Column(type="string",nullable=false)
     */
    private $password;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled;
    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     *
     */
    private $telepone;
    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     *
     */
    private $codePostal;
    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     *
     */
    private $region;
    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     *
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="Chark\ApiBundle\Entity\Coupon", mappedBy="client",cascade={"persist", "remove"})
     */
    private $coupon;

    /**
     * @ORM\OneToMany(targetEntity="Chark\ApiBundle\Entity\Panier", mappedBy="client", cascade={"persist", "remove"})
     */
    private $panier;


    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Client
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Client
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telepone
     *
     * @param string $telepone
     *
     * @return Client
     */
    public function setTelepone($telepone)
    {
        $this->telepone = $telepone;

        return $this;
    }

    /**
     * Get telepone
     *
     * @return string
     */
    public function getTelepone()
    {
        return $this->telepone;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return Client
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return integer
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return Client
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Client
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Add coupon
     *
     * @param Coupon $coupon
     *
     * @return Client
     */
    public function addCoupon(Coupon $coupon)
    {
        $this->coupon[] = $coupon;

        return $this;
    }

    /**
     * Remove coupon
     *
     * @param Coupon $coupon
     */
    public function removeCoupon(Coupon $coupon)
    {
        $this->coupon->removeElement($coupon);
    }

    /**
     * Get coupon
     *
     * @return Collection
     */
    public function getCoupon()
    {
        return $this->coupon;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->panier = new ArrayCollection();
        $this->coupon = new ArrayCollection();
        $this->enabled = true;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Client
     */
    public function setEnabled($enabled)
    {
        $this->enabled = (bool) $enabled;
        return $this;
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->prenom;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add panier
     *
     * @param Panier $panier
     *
     * @return Client
     */
    public function addPanier(Panier $panier)
    {
        $this->panier[] = $panier;

        return $this;
    }

    /**
     * Remove panier
     *
     * @param Panier $panier
     */
    public function removePanier(Panier $panier)
    {
        $this->panier->removeElement($panier);
    }

    /**
     * Get panier
     *
     * @return Collection
     */
    public function getPanier()
    {
        return $this->panier;
    }
}
