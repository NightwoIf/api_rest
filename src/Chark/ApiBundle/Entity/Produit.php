<?php

namespace Chark\ApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Exception;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="Chark\ApiBundle\Repository\ProduitRepository")
 * @Serializer\ExclusionPolicy("none")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_ttc", type="decimal", precision=10, scale=2)
     */
    private $prixTtc;

    /**
     * @ORM\ManyToOne(targetEntity="Categorie", inversedBy="produit")
     */
    private $categorie;

    /**
     * @OneToMany(targetEntity="Chark\ApiBundle\Entity\Image", mappedBy="produit", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $imagePath;

    /**
     *
     */
    private $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="Chark\ApiBundle\Entity\Marque", mappedBy="produits", cascade={"persist"})
     */
    private $marques;

    /**
     * @Gedmo\Slug(fields={"libelle"})
     * @ORM\Column(type="string",unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Chark\ApiBundle\Entity\Discount",inversedBy="products")
     */
    private $discount;


    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }


    /**
     * @return mixed
     */

    public function getImageFile()
    {
        return $this->imageFile;
    }


    /**
     * @param mixed $image
     * @throws Exception
     */

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new ArrayCollection();
        $this->marques = new ArrayCollection();
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Produit
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Produit
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set prixTtc
     *
     * @param string $prixTtc
     *
     * @return Produit
     */
    public function setPrixTtc($prixTtc)
    {
        $this->prixTtc = $prixTtc;

        return $this;
    }

    /**
     * Get prixTtc
     *
     * @return string
     */
    public function getPrixTtc()
    {
        return $this->prixTtc;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Set categorie
     *
     * @param $categorie
     *
     * @return Produit
     */
    public function setCategorie($categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }


    /**
     * Add image
     *
     * @param Image $image
     *
     * @return Produit
     */
    public function addImage(Image $image)
    {

        if(!$this->image->contains($image)){
            $this->image[] = $image;
            $image->setProduit($this);
        }

        return $this;
    }

    public function removeImage(Image $image)
    {
        if($this->image->contains($image)){
            $this->image->removeElement($image);
            if($image->getProduit() === $this){
                $image->setProduit(null);
            }
        }
    }

    /**
     * Get image
     *
     * @return Collection
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Produit
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->libelle;
    }



    /**
     * Add marque
     *
     * @param Marque $marque
     *
     * @return Produit
     */
    public function addMarque(Marque $marque)
    {
        if(!$this->marques->contains($marque)){
            $this->marques[] = $marque;
            $marque->addProduit($this);
        }

        return $this;
    }

    /**
     * Remove marque
     *
     * @param Marque $marque
     * @return Produit
     */
    public function removeMarque(Marque $marque)
    {
        if($this->marques->contains($marque)){
            $this->marques->removeElement($marque);
            $marque->removeProduit($this);
        }
        return $this;

    }

    /**
     * Get marques
     *
     * @return Collection
     */
    public function getMarques()
    {
        return $this->marques;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    /**
     * Set discount
     *
     * @param Discount $discount
     *
     * @return Produit
     */
    public function setDiscount(Discount $discount = null)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return Discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
