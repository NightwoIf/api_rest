<?php

namespace Chark\ApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * Panier
 *
 * @ORM\Table(name="panier")
 * @ORM\Entity(repositoryClass="Chark\ApiBundle\Repository\PanierRepository")
 */
class Panier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Chark\ApiBundle\Entity\PanierElements", mappedBy="panier",cascade={"persist", "remove"})
     */
    private $items;
    /**
     * @ORM\ManyToOne(targetEntity="Chark\ApiBundle\Entity\Client", inversedBy="panier")
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_livraison", type="string", length=255)
     */
    private $adresseLivraison;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=10, scale=2)
     */
    private $total;

    /**
     * @ORM\OneToOne(targetEntity="Chark\ApiBundle\Entity\CouponInfo")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="client", referencedColumnName="client"),
     *     @ORM\JoinColumn(name="coupon", referencedColumnName="coupon")
     * })
     */
    private $couponInfo;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $shipped;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $payed;
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->shipped = false;
        $this->payed = false;
        $this->createdAt = new DateTime('now');
        $this->items = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adresseLivraison
     *
     * @param string $adresseLivraison
     *
     * @return Panier
     */
    public function setAdresseLivraison($adresseLivraison)
    {
        $this->adresseLivraison = $adresseLivraison;

        return $this;
    }

    /**
     * Get adresseLivraison
     *
     * @return string
     */
    public function getAdresseLivraison()
    {
        return $this->adresseLivraison;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return Panier
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set couponInfo
     *
     * @param Couponinfo $couponInfo
     *
     * @return Panier
     */
    public function setCouponInfo(Couponinfo $couponInfo = null)
    {
        $this->couponInfo = $couponInfo;
        return $this;
    }

    /**
     * Get couponInfo
     *
     * @return Couponinfo
     */
    public function getCouponInfo()
    {
        return $this->couponInfo;
    }

    /**
     * @return bool
     */
    public function isShipped()
    {
        return $this->shipped;
    }

    /**
     * @param bool $shipped
     * @throws Exception
     */
    public function setShipped($shipped)
    {
        $this->shipped = $shipped;
        $this->updatedAt = new DateTime('now');
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * @param bool $payed
     * @throws Exception
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;
        $this->updatedAt = new DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * Get shipped
     *
     * @return boolean
     */
    public function getShipped()
    {
        return $this->shipped;
    }

    /**
     * Add item
     *
     * @param PanierElements $item
     *
     * @return Panier
     */
    public function addItem(PanierElements $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param PanierElements $item
     */
    public function removeItem(PanierElements $item)
    {
        $this->items->removeElement($item);
    }
}
