<?php

namespace Chark\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Coupon_info
 *
 * @ORM\Table(name="couponInfo")
 * @ORM\Entity(repositoryClass="Chark\ApiBundle\Repository\Coupon_infoRepository")
 */
class CouponInfo
{

    /**
     * @ORM\Id
     * @OneToOne(targetEntity="Chark\ApiBundle\Entity\Client")
     * @ORM\Column(name="client")
     */
    private $client;

    /**
     * @ORM\Id
     * @OneToOne(targetEntity="Chark\ApiBundle\Entity\Coupon")
     * @ORM\Column(name="coupon")
     */
    private $coupon;


    /**
     * Set client
     *
     * @param \Chark\ApiBundle\Entity\Client $client
     *
     * @return CouponInfo
     */
    public function setClient(\Chark\ApiBundle\Entity\Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Chark\ApiBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }


    /**
     * Set coupon
     *
     * @param \Chark\ApiBundle\Entity\Coupon $coupon
     *
     * @return Couponinfo
     */
    public function setCoupon(\Chark\ApiBundle\Entity\Coupon $coupon)
    {
        $this->coupon = $coupon;

        return $this;
    }

    /**
     * Get coupon
     *
     * @return \Chark\ApiBundle\Entity\Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }
}
