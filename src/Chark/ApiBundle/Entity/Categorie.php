<?php

namespace Chark\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="Chark\ApiBundle\Repository\CategorieRepository")
 * @Serializer\ExclusionPolicy("none")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_categorie", type="string", length=255)
     * @Serializer\Groups({"Default"})
     */
    private $nomCategorie;

    /**
     * @ORM\ManyToOne(targetEntity="Chark\ApiBundle\Entity\Categorie", inversedBy="sousCategorie")
     *
     */

    private $categorie;

    /**
     * @OneToMany(targetEntity="Chark\ApiBundle\Entity\Categorie",mappedBy="categorie")
     * @Serializer\Exclude()
     */

    private $sousCategorie;

    /**
     * @ORM\OneToMany(targetEntity="Chark\ApiBundle\Entity\Produit", mappedBy="categorie")
     * @Serializer\Exclude()
     */
    private $produit;

    /**
     * @Gedmo\Slug(fields={"nomCategorie"})
     * @ORM\Column(type="string",unique=true)
     */
    private $slug;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nomCategorie
     *
     * @param string $nomCategorie
     *
     * @return Categorie
     */
    public function setNomCategorie($nomCategorie)
    {
        $this->nomCategorie = $nomCategorie;

        return $this;
    }

    /**
     * Get nomCategorie
     *
     * @return string
     */
    public function getNomCategorie()
    {
        return $this->nomCategorie;
    }


    /**
     * Add produit
     *
     * @param \Chark\ApiBundle\Entity\Produit $produit
     *
     * @return Categorie
     */
    public function addProduit(\Chark\ApiBundle\Entity\Produit $produit)
    {
        $this->produit[] = $produit;

        return $this;
    }

    /**
     * Remove produit
     *
     * @param \Chark\ApiBundle\Entity\Produit $produit
     */
    public function removeProduit(\Chark\ApiBundle\Entity\Produit $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Get produit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Set categorie
     *
     * @param \Chark\ApiBundle\Entity\Categorie $categorie
     *
     * @return Categorie
     */
    public function setCategorie(\Chark\ApiBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Chark\ApiBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Add sousCategorie
     *
     * @param $sousCategorie
     *
     * @return Categorie
     */
    public function addSousCategorie($sousCategorie)
    {
        $this->sousCategorie[] = $sousCategorie;

        return $this;
    }

    /**
     * Remove sousCategorie
     *
     * @param \Chark\ApiBundle\Entity\Categorie $sousCategorie
     */
    public function removeSousCategorie(\Chark\ApiBundle\Entity\Categorie $sousCategorie)
    {
        $this->sousCategorie->removeElement($sousCategorie);
    }

    /**
     * Get sousCategorie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSousCategorie()
    {
        return $this->sousCategorie;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Categorie
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
