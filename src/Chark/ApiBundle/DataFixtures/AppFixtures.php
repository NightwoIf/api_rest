<?php


namespace Chark\ApiBundle\DataFixtures;


use Chark\ApiBundle\Entity\Categorie;
use Chark\ApiBundle\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // TODO: Implement load() method.

        for($i = 0; $i<20;$i++){
//            $categorie = $this->getReference(PrincipalFixtures::getReferenceKey($i % 5));
            $product = new Produit();
            $product->setLibelle('product'.$i);
            $product->setQuantite(rand(10,50));
            $product->setDescription('description'.$i);
            $product->setPrixTtc(rand(100,1000));
//            $product->setCategorie($categorie);
            $manager->persist($product);
        }
        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
//    function getDependencies()
//    {
//        // TODO: Implement getDependencies() method.
//        return array(
//        );
//    }
}