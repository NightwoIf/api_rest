<?php

namespace Chark\ApiBundle\Controller;

use Chark\ApiBundle\Representation\Produit;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Pagerfanta\Exception\LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CategorieController extends Controller
{
    /**
     * @Rest\Get("/Categories")
     * @Rest\QueryParam(
     *     name="keyword",
     *     requirements="[a-zA-Z0-9]+",
     *     nullable=true,
     *     description="The keyword to search for."
     * )
     * @Rest\QueryParam(
     *     name="order",
     *     requirements="asc|desc",
     *     default="asc",
     *     description="Sort order (asc or desc)."
     * )
     * @Rest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default="20",
     *     description="Max number of categories per page."
     * )
     * @Rest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default="0",
     * description="The pagination offset."
     * )
     * @Rest\View()
     * @param ParamFetcherInterface $paramFetcher
     * @return Categorie
     */
    public function getCategorieAction(ParamFetcherInterface $paramFetcher)
    {

        $repository = $this->getDoctrine()->getRepository('CharkApiBundle:Categorie');

        $categories = $repository->searchCategories();

        return $categories;


    }

}
