<?php

namespace Chark\ApiBundle\Controller;

use Chark\ApiBundle\Entity\Client;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ClientController extends Controller
{
    /**
     * @ApiDoc(
     *     description="Create a new user",
     *     parameters={{"name"="categoryId", "dataType"="integer", "required"=true, "description"="category id"}}
     * )
     * @Rest\Post("/register")
     * @Rest\View()
     * @ParamConverter("client", converter="fos_rest.request_body")
     * @param Client $client
     * @return View
     */
    public function signUpAction(Client $client)
    {
        $check = $this->getDoctrine()->getRepository(Client::class)->findOneBy([
            'email' => $client->getEmail(),
        ]);
        if(!$check){
            $em = $this->getDoctrine()->getManager();
            $encoded = sha1($client->getPassword());
            $client->setPassword($encoded);
            $em->persist($client);
            $em->flush();
            return View::create(null,201);
        }else{
            return View::create(null,403);
        }
    }

    /**
     * @ApiDoc()
     * @Rest\Post("/login")
     * @Rest\View()
     * @ParamConverter("client", converter="fos_rest.request_body")
     * @param Client $client
     * @return View
     */
    public function signInAction(Client $client)
    {
        $check = $this->getDoctrine()->getRepository(Client::class)->findOneBy([
            'email' => $client->getEmail(),
            'password' => sha1($client->getPassword()),
        ]);
        if($check){
            return $check;
        }else{
            return View::create(null,401);
        }
    }



}
