<?php

namespace Chark\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class MarqueController extends Controller
{
    /**
     * @Rest\Get("/Marques")
     * @Rest\View()
     */
    public function getMarqueAction()
    {

        $repository = $this->getDoctrine()->getRepository('CharkApiBundle:Marque');

        $marques = $repository->afficherMarques();

        return $marques;
    }

}
