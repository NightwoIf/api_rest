<?php

namespace Chark\ApiBundle\Controller;
use Chark\ApiBundle\Representation\Produit;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Pagerfanta\Exception\LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProduitController extends Controller
{
    /**
     * @ApiDoc(
     *     description="How to get the list of products as well as ordering them and searching them via a keyword query.",
     *     output= { "class"=\Chark\ApiBundle\Representation\Produit::class, "collection"=true, "groups"={"Default"} }
     * )
     * @Rest\Get("collection/produits", name="api_produits_list")
     * @Rest\QueryParam(
     *     name="keyword",
     *     requirements="[a-zA-Z0-9]+",
     *     nullable=true,
     *     description="the keyword to search for"
     * )
     * @Rest\QueryParam(
     *     map=true,
     *     name="marque",
     *     requirements="[a-zA-Z0-9]+",
     *     description="the marque to search for",
     *     nullable=true
     * )
     * @Rest\QueryParam(
     *     name="price",
     *     requirements="asc|desc",
     *     description="sorting products by price."
     * )
     * @Rest\QueryParam(
     *     name="name",
     *     requirements="asc|desc",
     *     description="sorting products by name."
     * )
     * @Rest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default="15",
     *     description="Max number per page"
     * )
     * @Rest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default="0",
     *     description="the pagination offset"
     * )
     * @Rest\View(serializerGroups={"Default"})
     * @param ParamFetcher $paramFetcher
     * @return Produit
     */
    public function listProductsAction(ParamFetcherInterface $paramFetcher){
        $products = $this->getDoctrine()->getRepository('CharkApiBundle:Produit')->searchByName(
            $paramFetcher->get('keyword'),
            $paramFetcher->get('marque'),
            $paramFetcher->get('name'),
            $paramFetcher->get('price'),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );
        return new Produit($products);
    }

    /**
     * @Rest\Get("/{slugCat}/produit/{slug}",name="api_get_produit")
     * @Rest\View(serializerGroups={"Details"})
     * @param $slug
     * @return array|LogicException
     */
    public function getProduct($slug){
        $produit = $this->getDoctrine()->getRepository('CharkApiBundle:Produit')
            ->getProductBySlug($slug);
        if(!is_object($produit)){
            return new LogicException();
        }
        return $produit;
    }

    /**
     * @Rest\Get("{catSlug}/produits")
     * @Rest\QueryParam(
     *     name="keyword",
     *     requirements="[a-zA-Z0-9]+",
     *     description="the keyword to search for",
     * )
     * @Rest\QueryParam(
     *     map=true,
     *     name="marque",
     *     requirements="[a-zA-Z0-9]+",
     *     description="the marque to search for",
     *     nullable=true
     * )
     * @Rest\QueryParam(
     *     name="price",
     *     requirements="asc|desc",
     *     description="sorting products by price."
     * )
     * @Rest\QueryParam(
     *     name="name",
     *     requirements="asc|desc",
     *     description="sorting products by name."
     * )
     * @Rest\QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default="15",
     *     description="Max number per page"
     * )
     * @Rest\QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default="0",
     *     description="the pagination offset"
     * )
     * @Rest\View(serializerGroups={"Default"})
     * @param $catSlug
     * @param ParamFetcherInterface $paramFetcher
     * @return Produit
     */

    public function productsByCategory($catSlug,ParamFetcherInterface $paramFetcher){
        $products = $this->getDoctrine()->getRepository('CharkApiBundle:Produit')
            ->searchProductsByCat(
                $paramFetcher->get('marque'),
                $catSlug,
                $paramFetcher->get('keyword'),
                $paramFetcher->get('name'),
                $paramFetcher->get('price'),
                $paramFetcher->get('limit'),
                $paramFetcher->get('offset'));

        return new Produit($products);

    }





}
