<?php

namespace Chark\ApiBundle\Repository;

use Doctrine\DBAL\Connection;

/**
 * ProduitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProduitRepository extends AbstractRepository
{
    public function searchByName($term,$marque,$order,$price,$limit = 20,$offset = 0)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->leftJoin('p.marques','m');
        if($term){
            if(empty($marque)){
                if($order){
                    $qb
                        ->where('p.libelle LIKE ?1')
                        ->setParameter(1, '%'.$term.'%')
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->where('p.libelle LIKE ?1')
                        ->setParameter(1, '%'.$term.'%')
                        ->orderBy('p.prixTtc',$order);
                }else{
                    $qb
                        ->andWhere('p.slug LIKE ?1')
                        ->setParameter(1,'%'.$term.'%');
                }
            }else{
                if($order){
                    $qb
                        ->where('m.slug IN (:nmarque)')
                        ->andWhere('p.libelle LIKE ?1')
                        ->setParameter(1, '%'.$term.'%')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->where('m.slug IN (:nmarque)')
                        ->andWhere('p.libelle LIKE ?1')
                        ->orderBy('p.prixTtc',$order)
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->setParameter(1, '%'.$term.'%');
                }else{
                    $qb
                        ->where('p.slug LIKE ?1')
                        ->Andwhere('m.slug IN (:nmarque)')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->setParameter(1,'%'.$term.'%');
                }
            }
        }else{
            if(empty($marque)){
                if($order){
                    $qb
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->orderBy('p.prixTtc',$order);
                }
            }else{
                if($order){
                    $qb
                        ->where('m.slug IN (:nmarque)')
                        ->orderBy('p.libelle',$order)
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY);
                }elseif ($price){
                    $qb
                        ->andWhere('m.slug in (:nmarque)')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->orderBy('p.prixTtc',$order);
                }else{
                    $qb
                        ->andWhere('m.slug in (:nmarque)')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY);
                }
            }
        }

        return $this->paginate($qb,$limit,$offset);

    }

    public function getProductBySlug($slug){

        $qb= $this->createQueryBuilder('p')
            ->where('p.slug = :slug')
            ->setParameter('slug',$slug)
            ->getQuery()
            ->getResult();

        return $qb;
    }
    public function searchProductsByCat($marque,$cat,$term,$order,$price,$limit = 20,$offset = 0){
        $qb = $this->createQueryBuilder('p')
            ->join('p.categorie','c')
            ->join('c.categorie','sous')
            ->leftJoin('p.marques','m')
            ->Where('c.slug = :cname')
            ->orWhere('sous.slug = :sousname')
            ->setParameter('cname',$cat)
            ->setParameter('sousname',$cat);
        if($term)
        {
            if(!empty($marque))
            {
                if($order){
                    $qb
                        ->andWhere('m.slug in (:nmarque)')
                        ->Andwhere('p.libelle LIKE ?1')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->setParameter(1, '%'.$term.'%')
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->andWhere('m.slug IN (:nmarque)')
                        ->Andwhere('p.libelle LIKE ?1')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->setParameter(1, '%'.$term.'%')
                        ->orderBy('p.prixTtc',$order);
                }
                $qb
                    ->Andwhere('m.slug IN (:nmarque)')
                    ->Andwhere('p.libelle LIKE ?1')
                    ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                    ->setParameter(1, '%'.$term.'%');
            }else{
                if($order){
                    $qb
                        ->Andwhere('p.libelle LIKE ?1')
                        ->setParameter(1, '%'.$term.'%')
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->Andwhere('p.libelle LIKE ?1')
                        ->setParameter(1, '%'.$term.'%')
                        ->orderBy('p.prixTtc',$order);
                }
                $qb
                    ->Andwhere('p.libelle LIKE ?1')
                    ->setParameter(1, '%'.$term.'%');
            }
        }else{
            if(!empty($marque))
            {
                if($order){
                    $qb
                        ->Andwhere('m.slug IN (:nmarque)')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->Andwhere('m.slug IN (:nmarque)')
                        ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY)
                        ->orderBy('p.prixTtc',$order);
                }
                $qb
                    ->Andwhere('m.slug IN (:nmarque)')
                    ->setParameter('nmarque',$marque, Connection::PARAM_STR_ARRAY);
            }else{
                if($order){
                    $qb
                        ->orderBy('p.libelle',$order);
                }elseif ($price){
                    $qb
                        ->orderBy('p.prixTtc',$order);
                }
            }
        }
        return $this->paginate($qb,$limit,$offset);
    }
}
