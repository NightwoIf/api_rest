<?php


namespace Chark\ApiBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

Abstract class AbstractRepository extends EntityRepository
{
    protected function paginate(QueryBuilder $qb,$max=20,$offset = 0)
    {
        if (0 == $max)
        {
           throw new \LogicException('$max must be greater than 0.');
        }
        $pager = new pagerfanta(new DoctrineORMAdapter($qb));
        $current = ceil(($offset+1)/$max);
        $pager->setCurrentPage($current);
        $pager->setMaxPerPage((int)$max);
        return $pager;
    }

}